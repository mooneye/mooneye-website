const gulp = require('gulp'),
  gulpif = require('gulp-if'),
  gutil = require('gulp-util'),
  uglify = require('gulp-uglify'),
  uglifycss = require('gulp-uglifycss'),
  less = require('gulp-less'),
  concat = require('gulp-concat'),
  sourcemaps = require('gulp-sourcemaps'),
  tar = require('gulp-tar'),
  gzip = require('gulp-gzip'),
  jshint = require('gulp-jshint'),
  exec = require('gulp-exec'),
  env = process.env.GULP_ENV,
  nodeModulePath = process.env.PWD + "/node_modules/",
  basePath = process.env.PWD + '/web/bundles/mooneye/';
  // runOptions = {
  //   continueOnError: true, // default = false, true means don't emit error event
  //   pipeStdout: false // default = false, true means stdout is written to file.contents
  // },
  // reportOptions = {
  //   err: true, // default = true, false means don't write err
  //   stderr: true, // default = true, false means don't write stderr
  //   stdout: true // default = true, false means don't write stdout
  // };

//GLOBAL JAVASCRIPT TASK: write one minified js file out of jquery.js, bootstrap.js and custom js files
gulp.task('js_dependencies', function() {
  const jquery = [nodeModulePath + 'jquery/dist/jquery.min.js'],
    bootstrap = [
      nodeModulePath + 'bootstrap/dist/js/bootstrap.min.js',
      nodeModulePath + 'bootstrap-switch/dist/js/bootstrap-switch.js'
    ],
    charts = [
      nodeModulePath + 'chart.js/dist/Chart.bundle.min.js',
      nodeModulePath + 'chart.js/dist/Chart.min.js'
    ],
    dateTimePicker = [
      nodeModulePath + 'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
    ],
    d3Charts = [
      nodeModulePath + 'd3/build/d3.min.js'
    ],
    highcharts = [
      nodeModulePath + 'highcharts/highcharts.js',
      nodeModulePath + 'highcharts/modules/exporting.js',
      nodeModulePath + 'highcharts/modules/offline-exporting.js',
      nodeModulePath + 'highcharts-canvas-tools/canvas-tools.js',
      nodeModulePath + 'highcharts-export-csv/export-csv.js',
      nodeModulePath + 'jspdf/dist/jspdf.min.js'
    ],
    moment = [
      nodeModulePath + 'moment/min/moment.min.js'
    ],
    utility = [
      'src/MooneyeBundle/Resources/public/js/gaia.js',
      'src/MooneyeBundle/Resources/public/js/modernizr.js'
    ],
    javaScripts = jquery.concat(bootstrap, charts, d3Charts, dateTimePicker, highcharts, moment, utility);

  return gulp.src(javaScripts)
    .pipe(concat('vendors.js'))
    .pipe(gulpif(env === 'prod', uglify()))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(basePath + 'js'));
});

gulp.task('install:assets', function() {
  return gulp.src('./src/MooneyeBundle/Resources/public/**/*')
    .pipe(gulp.dest(basePath));
});

//This lib needs an own file so that it can explicitly be referenced to by Highcharts (needs URL)
gulp.task('canvas-tools', function() {
  return gulp.src(nodeModulePath + 'highcharts-canvas-tools/canvas-tools.js')
});

// gulp.task('react:build:dev', function() {
//   return gulp.src('./src/MooneyeBundle/Resources/react/root.js')
//     .pipe(exec('npm run react:build:dev', runOptions))
//     .pipe(exec.reporter(reportOptions));
// });
//
// gulp.task('react:build:prod', function() {
//   return gulp.src('./src/MooneyeBundle/Resources/react/root.js')
//     .pipe(exec('npm run react:build:prod', runOptions))
//     .pipe(exec.reporter(reportOptions));
// });

// gulp.task('react:load:configs', function() {
//   return gulp.src('./src/MooneyeBundle/Resources/public/reactConfiguration/**/*.js')
//     .pipe(gulp.dest(basePath + 'reactConfiguration'));
// });

//SINGLE JAVASCRIPT TASK: write all javascript files as their own instance so they can be called with require
gulp.task('js', function() {
  return gulp.src('src/MooneyeBundle/Resources/public/js/**/*.js')
    .pipe(gulp.dest(basePath + 'js'));
});

gulp.task('jshint', function() {
  return gulp
    .src([
      'src/MooneyeBundle/Resources/public/js/*.js'
    ])
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('bootstrap', function() {
  const bootstrap = [
    nodeModulePath + 'bootstrap/less/**/*'
  ];

  return gulp.src(bootstrap)
    .pipe(gulp.dest(basePath + 'css/less/bootstrap' ));
});

gulp.task('css', ['bootstrap'], function() {
  var intern = [basePath + 'css/less/master.less'],
    dependencies = [
      nodeModulePath + 'bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css',
      nodeModulePath + 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css'
    ];

  var css = intern.concat(dependencies);

  return gulp.src(css)
    .pipe(gulpif(/[.]less/, less()))
    .pipe(concat('styles.css'))
    .pipe(gulpif(env === 'prod', uglifycss()))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(basePath + 'css'));
});

//IMAGE TASK: Compile all Images as their own instance
gulp.task('img', function() {
  return gulp.src('src/MooneyeBundle/Resources/public/img/**/*.*')
    .pipe(gulp.dest(basePath + 'img'));
});

//FONTS TASK: Compile all fonts as their own instance
gulp.task('fonts', function() {
  const fonts = [],
    bootstrapFonts = [nodeModulePath + 'bootstrap/fonts/*.*'],
    customFonts = ['src/MooneyeBundle/Resources/public/fonts/**/*.*'],
    fontContainer = fonts.concat(bootstrapFonts, customFonts);
  return gulp.src(fontContainer)
    .pipe(gulp.dest(basePath + 'fonts'));
});

//WATCH TASK: Reload JS and LESS when they change
gulp.task('watch:new', function() {
  const onChange = function(event) {
    gutil.log(gutil.colors.magenta('File', event.type, 'at', event.path));
  };
  gulp.watch('./src/MooneyeBundle/Resources/css/less/**/*.less', ['css'])
    .on('change', onChange);

  gulp.watch([
    'src/MooneyeBundle/Resources/public/js/**/*.js'
  ], ['js', 'js_dependencies'])
    .on('change', onChange);
});

gulp.task('tarball', ['default'], function() {
  gutil.log(gutil.colors.magenta('Started packaging a tarball.'));
  gulp.src([
    'app/**',
    '!app/config/parameters.yml',
    // '!app/config/parameters.codeship.yml',
    'src/**',
    basePath + 'js/**',
    // basePath + 'reactConfiguration/**',
    basePath + 'css/**',
    basePath + 'img/**',
    basePath + 'fonts/**',
    'vendor/**',
    'web/*',
    'web/.htaccess',
    '!web/app_dev.php',
    'bin/console'
  ], {'base': '.'})
    .pipe(tar('deployme.tar'))
    .pipe(gzip())
    .pipe(gulp.dest('.'));
  gutil.log(gutil.colors.magenta('Saved tarball using the name deployme.tar.gz'));
});

gulp.task('build', [
  'tarball'
]);

gulp.task('default', [
  'install:assets',
  'js_dependencies',
  'canvas-tools',
  'js',
  'css',
  'fonts',
  'img'
]);
