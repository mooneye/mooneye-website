<?php
namespace MooneyeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('mooneye');

//        $rootNode
//            ->children()
//            ->variableNode('formdefaults')->isRequired()->end()
//            ->end();

        return $treeBuilder;
    }
}