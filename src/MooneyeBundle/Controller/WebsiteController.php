<?php

namespace MooneyeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route(service="mooneye.controller.website")
 */
class WebsiteController
{
    /**
     * @var Translator
     */
    private $translator;

    /**
     * WebsiteController constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        dump($request);
        return [];
    }
}
