# www.robotstxt.org/
# www.google.com/support/webmasters/bin/answer.py?hl=en&answer=156449

User-agent: *
Disallow:
Sitemap: http://www.mooneye.de/sitemap.xml

#disallow all
#Disallow: /

#disallow custom file or image
#Disallow: /beispieldatei.html
#Disallow: /images/beispielbild.jpg

#disallow all images
#Disallow: /*.jpg$

#disallow folder but allow one subfolder
#Disallow: /shop/
#Allow: /shop/magazin/
